# Auspex
# Novos Devs

Se chegou até aqui é porque está querendo entrar para nosso time, certo? Aqui na Auspex trabalhamos diariamente tentando superar nossos objetivos e sempre entregar o melhor para nossos clientes.
Estamos sempre buscando novas tecnologias e soluções para aumentar nossa produtividade, então aprender coisas novas faz parte da nossa rotina.
Prezamos muito por um ambiente de trabalho leve e descontraído e sabemos que o elemento chave para isso é o comprometimento de cada um com suas responsabilidades e com o grupo.

E aí, se identificou com a gente? Então vamos lá, já temos o primeiro desafio!

# Desafio
Antes de entrar para o nosso time gostaríamos de conhecer um pouco de suas habilidades com programação. Vamos estipular algumas regras, ok:

- Use tudo como se fosse um projeto seu para o mercado, chame um amigo, pegue um componente, pesquise no Google.
- Não admitimos trapaça, temos certeza que saberá diferenciar o item acima desse.
- Preferencialmente use a plataforma para a qual se candidatou. Se a intenção é aprender e ainda não conhece, não tem problema, use a plataforma que preferir.
- Não queremos produtos perfeitos, apenas conhecer um pouco como trabalha. Se gastar mais de 4h, então já é mais do que esperávamos tomar do seu tempo.

# Escopo 

Escolha a plataforma que deseja se candidatar e comece seu projeto.

Precisamos de uma ferramenta para o controle de tarefas a fazer, o famoso TODO. 

Dica: Capriche na usabilidade que pode ser seu diferencial.

A aplicação deve cumprir os seguintes requisitos:

## Web ou Mobile (Android Kotlin / iOS (Swift))

- A tela principal deve ser a minha lista de tarefas
- Permitir que o usuário consiga facilmente adicionar uma nova tarefa
- Possibilitar a finalização de uma tarefa e poder desfazer isso
- Facilitar a visualização das atividades pendentes e finalizadas separadamente
- O usuário pode excluir uma tarefa, mas garantir que ele tenha certeza disso
- Manter o estado do app (fechar e abrir o app e as informações ainda devem estar disponíveis)

# Entrega

Para entregar você deve:

- Fazer um Fork desse projeto
- Desenvolver sua aplicação utilizando o Git
- Abrir um PullRequest com o seu código.

Mão na massa e boa sorte!